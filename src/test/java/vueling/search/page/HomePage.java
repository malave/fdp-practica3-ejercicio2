package vueling.search.page;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vueling.search.domain.Constants;
import vueling.search.domain.SearchFormDto;

import java.lang.invoke.MethodHandles;

public class HomePage extends PageObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @FindBy(id = Constants.ID_ACCEPT_COOKIES)
    private WebElementFacade acceptCookies;

    @FindBy(css = Constants.SELECTOR_FROM)
    private WebElementFacade from;

    @FindBy(css = Constants.SELECTOR_FROM_OPTION)
    private WebElementFacade fromOption;

    @FindBy(css = Constants.SELECTOR_TO)
    private WebElementFacade to;

    @FindBy(css = Constants.SELECTOR_TO_OPTION)
    private WebElementFacade toOption;

    @FindBy(id = Constants.ID_SEARCH_BUTTON)
    private WebElementFacade searchButton;

    @FindBy(css = Constants.SELECTOR_ONE_WAY_OPTION)
    private WebElementFacade oneWayOption;

    public void navigate() {
        this.getDriver().get(Constants.URL_HOME);
        acceptCookies.waitUntilVisible().click();
    }

    public void search(SearchFormDto flightQuery) {
        setFrom(flightQuery.getFrom());
        setTo(flightQuery.getTo());
        if (flightQuery.getReturnDate() == null) {
            oneWayOption.waitUntilVisible().click();
        }
//        Set passenger number
        searchButton.click();
    }

    private void setFrom(String originPlace) {
        from.click();
        from.sendKeys(originPlace);
        fromOption.click();
    }

    private void setTo(String destinationPlace) {
        to.click();
        to.sendKeys(destinationPlace);
        toOption.click();
    }
}
