package vueling.search.page;

import net.serenitybdd.core.pages.PageObject;
import vueling.search.domain.Constants;

public class ResultPage extends PageObject {

    public boolean validate() {
        return this.getDriver().getCurrentUrl().equals(Constants.URL_RESULT);
    }
}
