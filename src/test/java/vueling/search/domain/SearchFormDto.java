package vueling.search.domain;

public class SearchFormDto {
    private final String from;
    private final String to;
    private final String outboundDate;
    private final String returnDate;
    private final String passengers;

    public SearchFormDto(String from,
                         String to,
                         String outboundDate,
                         String returnDate,
                         String passengers
    ) {
        this.from = from;
        this.to = to;
        this.outboundDate = outboundDate;
        this.returnDate = returnDate;
        this.passengers = passengers;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getOutboundDate() {
        return outboundDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getPassengers() {
        return passengers;
    }

}
