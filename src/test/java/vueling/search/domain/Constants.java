package vueling.search.domain;

public final class Constants {

    private Constants() {
    }

    public static final String URL_HOME = "https://www.vueling.com/es";
    public static final String URL_RESULT = "https://tickets.vueling.com/ScheduleSelectNew.aspx";
    public static final String ID_ACCEPT_COOKIES = "ensCloseBanner";
    public static final String SELECTOR_FROM = "#tab-search > div > div.form-group.form-group--flight-search > vy-airport-selector.form-input.origin > div > input";
    public static final String SELECTOR_FROM_OPTION = "#popup-list > vy-airports-li > li > p:nth-child(1)";
    public static final String SELECTOR_TO = "#tab-search > div > div.form-group.form-group--flight-search > vy-airport-selector.form-input.destination > div > input";
    public static final String SELECTOR_TO_OPTION = "#popup-list > vy-airports-li > li.liStation > p:nth-child(1)";
    public static final String SELECTOR_ONE_WAY_OPTION = "#searchbar > div > vy-datepicker-popup > vy-datepicker-header > ul > li:nth-child(2) > label";
    public static final String SELECTOR_DATE = "#searchbar > div > vy-datepicker-popup > vy-specificdates-datepicker > div > div.ui-datepicker-group.ui-datepicker-group-last > table > tbody > tr:nth-child(1) > td:nth-child(6) > a"; //May 1 2021
    public static final String ID_SEARCH_BUTTON = "btnSubmitHomeSearcher";
    public static final String SELECTOR_HEADER_TITLE = "#newStv > div.stv-journey > div > div.vy-journey_header > h3";
}
