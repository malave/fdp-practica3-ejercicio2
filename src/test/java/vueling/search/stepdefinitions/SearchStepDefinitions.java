package vueling.search.stepdefinitions;

import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import vueling.search.domain.SearchFormDto;
import vueling.search.page.HomePage;
import vueling.search.page.ResultPage;

import java.util.List;
import java.util.Map;

public class SearchStepDefinitions {
    private HomePage homePage;
    private ResultPage resultPage;


    @DataTableType
    public SearchFormDto searchDtoDataTableTransform(Map<String, String> entry) {
        return new SearchFormDto(
                entry.get("origin"),
                entry.get("destination"),
                entry.get("outboundDate"),
                entry.get("returnDate"),
                entry.get("passengers")
        );
    }

    @Given("I'm main page")
    public void navigateToHomePage() {
        homePage.navigate();
    }

    @When("I try to find a fly")
    public void search(List<SearchFormDto> data) {
        SearchFormDto searchForm = data.get(0);
        homePage.search(searchForm);
    }

    @Then("I get available flight")
    public void validateSearchResults() {
        Assert.assertTrue(resultPage.validate());
    }
}
